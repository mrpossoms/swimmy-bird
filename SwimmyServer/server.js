var events = require('events');
var emitter = new events.EventEmitter();
var http  = require('http');
var mysql = require('mysql');
var con   = mysql.createConnection({
	host:     'localhost',
	user:     'root',
	password: 'password'
});

var port = process.argv[2] || 8080;

function parseQueryString(url){
	var query = url.substr(1, url.length).split("?");
	var kvp = query[query.length-1].split("&");
	var table = {};
	for(var p in kvp){
		p = kvp[p];
		var pair = p.split("=");
		table[pair[0]] = pair[1];
	}
	return table;
}

con.query('USE swimmy_bird_scores', function(err, rows, fields){
	if(err) throw err;

	//emitter.emit('connectionOpen', con, com);
});


http.createServer(function(req, res){

	// communication object (request, response)
	var com = {
		req: req,
		res: res
	};

	res.setHeader('Access-Control-Allow-Origin', 'http://swimmybird.com');
	res.setHeader('Access-Control-Allow-Origin', 'http://www.swimmybird.com');
	res.setHeader('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
	res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
	res.writeHead(200);


	emitter.emit('request', con, com);
	
}).listen(parseInt(port, 10));

emitter.on('request', function(con, com){
	var res = com.res;
	var args = parseQueryString(com.req.url);
		
	if(args['act'])
	switch(args['act']){
		case 'flap':
			if(!args['email'] /*|| !args['pass']*/)
				emitter.emit('requestError', res, 'Missing email and/or password');

			var user = {
				email: args['email'],
				pass: args['pass']
			};
			emitter.emit('incScore', con, com, user);
			break;
		case 'done':
                        if(!args['email'] /*|| !args['pass']*/){
                                emitter.emit('requestError', res, 'Missing email and/or password');
				return;
			}

                        var user = {
                                email: args['email'],
                                pass: args['pass']
                        };
                        emitter.emit('gameOver', con, com, user);

			break;
		case 'signup':
			if(!args['email'] || /*!args['pass'] ||*/ !args['name']){
				emitter.emit('requestError', res, 'Missing email, name or password');
				return;
			}
			emitter.emit(
				'requestNewUser',
				con,
				com,
				{
					email: args['email'],
					pass:  args['pass'],
					name:  args['name']
				}
			);
			break;
		default: // not recognized action
			emitter.emit(
				'requestError',
				res,
				'Request action "' + args['act'] + '" not recognized.'
			);
			break;
	}	
	else{ // no action in query
		emitter.emit('requestError', res, 'Request action missing in query!');
	}


});

emitter.on('requestError', function(res, msg){
	res.write(JSON.stringify({
		type: 'requestError',
		message: msg 
	}));
	res.end();
});

emitter.on('requestSuccess', function(res, data){
	res.write(JSON.stringify(data));
	res.end();

});

emitter.on('requestNewUser', function(con, com, user){
	var sql = 'INSERT INTO Users VALUES("' + 
		user.email + '","' +
		user.pass  + '","' +
		user.name  + '",' +
		'0,0,0,NOW())';

	console.log(sql);

	con.query(sql, function(err, rows, fields){
		if(err){
			emitter.emit('requestError', com.res, err.message);
			return;
		}
		
		else if(rows.affectedRows == 1){
			delete user.pass; // don't send the password back
			emitter.emit('requestSuccess', com.res, user);
			return;
		}
		else{
			emitter.emit('requestError', com.res, 'Unexpected insertion result');
		}
	});
});

emitter.on('gameOver', function(con, com, user){
	var res       = com.res;
	var condition = 'Email="' + user.email + '"';// AND Password="' + user.pass + '"';
	var sql       = 'SELECT * FROM Users WHERE ' + condition;
 
	con.query(sql, function(err, rows, fields){
		if(err){
			throw err;
		}

		if(rows.length == 1){
			var elapsed = new Date().getTime() - rows[0].LastPoint.getTime();

			// if it has taken too long, reset
			if(elapsed > 10000) rows[0].Currscore = 0;

			console.log(rows[0]);

			// if the new score isn't a high score
			if(rows[0].Currscore < rows[0].Highscore){
				//TODO leaderboard
				emitter.emit('requestSuccess', com.res, {});
				return;
			}

			// reset the curr score if they move faster than they should
			var scores = {
				highscore: rows[0].Currscore,
 				currscore: 0
			};

			// update the current score, and the time the last point was set
			var sql = 'UPDATE Users SET LastPoint=NOW(), Highscore=' + scores.highscore +
				', Currscore=0 WHERE ' + condition;

			con.query(sql, function(err, rows, fields){
				if(err){
					emitter.emit('requestError', res, err.message);
					return;
				}

				emitter.emit('requestSuccess', com.res, scores);
				
			}); 
		}
		else{
			emitter.emit(
				'requestError',
				res,
				'Unexpected query results: ' + rows.length + ' rows'
			);
		}
	});
});

emitter.on('incScore', function(con, com, user){
	var res       = com.res;
	var condition = 'Email="' + user.email + '"';// AND Password="' + user.pass + '"';
	var sql       = 'SELECT * FROM Users WHERE ' + condition;
 
	con.query(sql, function(err, rows, fields){
		if(err){
			throw err;
		}

		if(rows.length == 1){
			var elapsed = new Date().getTime() - rows[0].LastPoint.getTime();

			// if it has taken too long, reset
			if(elapsed > 10000) rows[0].Currscore = 0;

			// reset the curr score if they move faster than they should
			var scores = {
				highscore: rows[0].Highscore,
 				currscore: elapsed < 1800 ? rows[0].Currscore : rows[0].Currscore + 1
			};

			// update the current score, and the time the last point was set
			con.query(
				'UPDATE Users SET LastPoint=NOW(), Currscore=' + scores.currscore +
				' WHERE ' + condition,
				function(err, rows, fields){
					if(err){
						emitter.emit('requestError', res, err.message);
					}
					else{
						emitter.emit('requestSuccess', com.res, scores);
					}
				}
			); 
		}
		else{
			emitter.emit(
				'requestError',
				res,
				'Unexpected query results: ' + rows.length + ' rows'
			);
		}
	});
});
