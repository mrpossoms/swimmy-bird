function DrawSprite(sprite, x, y, w, h){
	$CTX.drawImage(
		spritesheet,
		sprite.x,
		sprite.y,
		sprite.w,
		sprite.h,
		x, y, w, h	
	);
}

function Email(){
	var email = $('#email').val();

	if(email != ''){
		return 'email='+email;
	}

	return null;
}

function User(){
	var email = $('#email').val();
	var name = $('#name').val();

	if(email != '' && name != ''){
		return 'email='+email+'&name='+name;
	}

	return null;
}

function Signup(){
	if(!User()){
		alert("Please enter you email and display name!");
		return;
	}

	gameStarted = true;
	
	$.ajax({
		url:'http://swimmybird.com:8080?act=signup&' + User() + '&pass=',
		datatype: 'jsonp',
		success: function(data){
		try{
		//Score = JSON.parse(data).currscore;
		}
		catch(e){}
		},
		error: function(data){
		try{
		console.log(JSON.parse(data.responseText));
		}
		catch(e){}
		}
	});

}

function James(){
	this.y = 240 - 32;
	this.x = 100;
	this.dy = 0;
	this.h = this.w = 32;

	this.center = {x: this.x + this.w/2,
				   y: this.y + this.h/2};

	this.Flap = function(){
		// TODO swim down
		this.dy += 200;

		if(!gameLost){
			swim[parseInt(Math.random() * 6)].Play();
		}
		flapped = 6;
	};

	this.Update = function(dt){
		//add the dy from flaps to the y value
		this.y += (this.dy * dt);

		// TODO float up
		//grabbity calculation
		this.dy -= (300 * dt);

		//update the y value of james's center
		this.center.y = this.y + this.h/2;
	};

	this.Draw = function(){
		if(this.y > $CVS.height - this.h){
			this.y = $CVS.height - this.h;
			this.dy = 0;
		}

		if(this.y < 110){
			this.y = 110;
			this.dy = 0;
		}

		DrawSprite(flapped > 0 ? LolJamesFlap : LolJames, this.x, this.y, this.w, this.h)
	};
}

function Iceburg(y){
	this.y = y;
	this.x = 640; // start at the right	
	this.scored = false;
	this.gapHeight = 64;
	this.w = 100;
	
	
	//initialize iceburg top and bottom x values to the iceburgs x value   75

	this.IceburgTopY = 75;	
	this.IceburgBottomY = this.y + this.gapHeight/2

	this.IceburgTopUnderWaterHeight = (this.y - this.gapHeight/2) - 75;
	this.IceburgBottomHeight = $CVS.height - this.IceburgBottomY;

	this.Advance = function(dt){
		// TODO move toward player
		this.x -= (180 * dt);
	};

	this.Score = function(){
		if(this.scored) return;
		this.scored = true;
		++Score;	
		bading.Play();
	};	

	this.Update = function(dt){
		//advance the iceburg
		this.Advance(dt);
	};

	this.Draw = function(){
		//DrawSprite(IceburgBackground, this.x, 0, this.w, 480)
		DrawSprite(IceburgTopUnderWater, this.x, this.IceburgTopY, this.w, this.IceburgTopUnderWaterHeight);
		DrawSprite(IceburgTopAboveWater, this.x, 0, this.w, 75);
		DrawSprite(IceburgBottom, this.x, this.IceburgBottomY, this.w, this.IceburgBottomHeight);
	};

	this.IsColliding = function(james){
		var x = james.center.x, y = james.center.y;
		var ix = this.x, iy = this.IceburgTopY;
		var ih = this.IceburgTopUnderWaterHeight;
		var slope = ((y - 50) - iy) / (ih / 50);
	
		if(x > ix && x < ix + 100) 
			this.Score();

		//check colliding with top iceburg piece
		if(x > ix + slope && x < ix + 100 - slope){
			if(y > iy && y < iy + ih){
				return true;
			}
		}

		

		//check colliding with bottom iceburg piece
		iy = this.IceburgBottomY;
		if(x > ix + 10 && x < ix + 40)
			if(y > iy && y < iy + this.IceburgBottomHeight){
				return true;
			}

		return false;
	}
}

function DrawScore(){
	var c = $CTX;
	var str = Score.toString();	

	c.font = '30pt Arial';
	c.fillStyle='#00cdeb';
	c.strokeStyle = '#000';

	c.lineWidth = 4;
	c.strokeStyle = '#FFF';
	c.strokeText(str, 320, 100);

	c.lineWidth = 1;
	c.strokeStyle = '#000';
	c.strokeText(str, 320, 100);

	c.fillText(str, 320, 100);	

}

function DrawTitle(){
	DrawSprite(SpriteTitle, 160, 187, 320, 106);
	DrawSprite(SpriteInstructions, 290, 300, 64, 32);	
	titleVisible = true;
}

function DrawReset(){
	DrawSprite(SpriteReset, 256, 224, 128, 32);
}

function DrawBackground(x){
	// TODO draw background, displaced x units
	for(var i = 8; i--;)
		DrawSprite(SpriteBackground, 640 - (120 * i + x) % 760, 0, 120, 480);
}

// TODO global vars
var spritesheet = new Image();

function Entry(){
	spritesheet.src = "assets/images/spritesheet.png";
	spritesheet.onload = function(){
		// called when loaded
		Game();
	};
}

function ResetGame(){
	james = new James();
	iceburgs = [];
	Score = 0;

	gameStarted = false;
	gameLost = false;
	canReset = true;
}

var james = new James();
var iceburgs = [];
var Score = 0;
var inited = false;
var gameStarted = false;
var gameLost = false;
var timeGameEnded;
var canReset = true;
var x = 0;
var bading, crash;
var swim = [];
var flapped = 0;

function init(){
	document.onclick = function() {
		james.Flap();

		if(!gameStarted && canReset){
			canReset = false;
			gameLost = false;
			gameStarted = true;
		}else if(canReset){
			ResetGame();
		}
	}

	//wire up sounds
	bading = $SB("assets/sounds/ba-ding.wav", 1);
	crash = $SB("assets/sounds/crash.wav", 1);
	for(var  i = 6; i--;){
		swim.push($SB("assets/sounds/swim" + i + ".wav", 2));
	}

	// Make sure shit is all pixely
	$CTX.imageSmoothingEnabled = false;
	$CTX.mozImageSmoothingEnabled = false;
	$CTX.webkitImageSmoothingEnabled = false;

	inited = true;
}

function Game(){
	// init crap
	// load res

	// register game loop
	OPfoundation.Init('canvas', function(){
	    if(!inited){
	    	init();
	    }

		var dt = $T.Tick() / 1000.0;
		gameLoop(dt);
	});

	var gameLoop = function(dt){
		// TODO game logic here
		if($KB.WasKeyPressed(KEY_DOWN)){
			james.Flap();

			if(!gameStarted && canReset){
				gameStarted = true;
				canReset = false;
				gameLost = false;
			}
			else if(canReset){
				ResetGame();
			}
		}

		if(gameStarted && !gameLost){
			//update james's position
			james.Update(dt);

			//check if we need a new iceburg
			var lastIceburg = iceburgs.Last();

			if(!lastIceburg || lastIceburg.x < 320){
				var gapCenter = (Math.random() * 200)+200;
				iceburgs.Enqueue(new Iceburg(gapCenter));
			} 

			//update iceburg positions
			for(var i = iceburgs.length; i--;){
				gameLost = iceburgs[i].IsColliding(james)

				if(gameLost){
					crash.Play();
					timeGameEnded = dt;
					break;
				}

				iceburgs[i].Update(dt);

				//check if we need to remove the iceburg form the array
				if(iceburgs[i].x + iceburgs[i].w < 0){
					iceburgs.Dequeue(i);
				}
			}

			// TODO make draw calls here
			$CTX.fillStyle = '#000';
			$CTX.fillRect(0, 0, 640, 480);

			DrawBackground(x++);

			for(var i = 0; i < iceburgs.length; i++){
				iceburgs[i].Draw();
			}

			//draw james
			james.Draw();

			DrawScore();
		}
		else if(gameLost){
			DrawBackground(x);

			for(var i = 0; i < iceburgs.length; i++){
				iceburgs[i].Draw();
			}

			//draw james
			james.Draw();	
			DrawScore();
			DrawReset();

			timeGameEnded += dt

			if (timeGameEnded >= 0.5){				
				canReset = true;			
			}		
		}else{
			DrawBackground(x++);

			//draw james
			james.Draw();

			DrawTitle();
		}

		if(flapped) --flapped;
	};
}
