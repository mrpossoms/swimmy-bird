SpriteBackground = {
	x: 0,
	y: 0,
	w: 32,
	h: 128
};

SpriteTitle = {
	x: 33,
	y: 0,
	w: 95,
	h: 32
};

SpriteReset = {
	x: 33,
	y: 112,
	w: 64,
	h: 16
};

SpriteInstructions = {
	x: 65,
	y: 96,
	w: 32,
	h: 16
};

SpriteTest = {
	x: 0,
	y: 0,
	w: 64,
	h: 64
};

LolJames = {
	x: 33,
	y: 96,
	w: 16,
	h: 16
}

LolJamesFlap = {
	x: 49,
	y: 96,
	w: 16,
	h: 16
}

IceburgTopAboveWater = {
	x: 97,
	y: 32,
	w: 31,
	h: 32
}

IceburgTopUnderWater = {
	x: 33,
	y: 32,
	w: 32,
	h: 64
}

IceburgBottom = {
	x: 65,
	y: 32,
	w: 32,
	h: 64
}

IceburgBackground = {
	x: 33,
	y: 32,
	w: 32,
	h: 64
}

